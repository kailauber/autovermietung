# Autovermietung Kai Lauber



## Allgemeine Projektangaben

Dies ist eine Konsolenapplikation für eine Autovermietung. Die Applikation kann folgendes tun:
- Fahrzeug in Garage einfügen 
- Fahrzeugkennzeichen anzeigen lassen 
- Fahrzeugstatus ändern 
- Räder aufpumpen 
- Energiequelle auffüllen 
- Vollständige Fahrzeugdaten anzeigen 
- Fahrzeug mieten 

### Technische Rahmenbedingungen

Folgendes wurde verwendet:

- Visual Studio 2019 (IDE)
- Gitlab
- Git


### Installation

Laden Sie einfach das Projekt als .zip herunter und führen Sie die .sln Datei aus.

Um die Daten abspeichert zu können, muss Visual Studio mit Administrator Rechten ausgeführt werden.

Ausserdem muss im Verzeichnis C:\ ein Ordner "carrental" erstellt werden. 

### Datenspeicherung

Die Daten werden in einer Datei persistiert. 
