﻿namespace ConsoleUI
{
    public class Program
    {
        public static void Main()
        {
            GarageManagement garageManagement = new GarageManagement();

            garageManagement.Run();
        }
    }
}