﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using GarageLogic;
using ConsoleUI;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTests
    {
        public void Main(string[] args)
        {
            writeFileTest();
            calculateRentcostTest();
        }

        [TestMethod]
        public void writeFileTest()
        {
            string licensePlate = "1";

            using (System.IO.StreamReader file =
            new System.IO.StreamReader($@"C:\carrental\licenseplate_{licensePlate}.txt", true))
            {
                while (true)
                {
                    string line = file.ReadLine();
                    if (line == null)
                    {
                        break;
                    }
                    Console.WriteLine(line); // Use line.
                }
            }
        }
        [TestMethod]
        public void calculateRentcostTest()
        {
            int price = 100;
            int days = 10;

            int total = price * days;
            if (total != 1000)
            {
                Console.WriteLine(total);
                Console.WriteLine("This is not the right answer!");

            }
            else
            {
                Console.WriteLine(total);
                Console.WriteLine("This is the right answer!");
            }

        }
    }
}